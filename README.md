# blog_os
#### 1. freestanding-rust-binary
how to start at first
```bash

# run at first
rustup target add thumbv7em-none-eabihf

cargo build --target thumbv7em-none-eabihf

```

#### 2. minimal-rust-kernel
use nightly rustc
```bash
rustup override set nightly

# check it
# the version should end with -nightly
rustc --version
```
install llvm-tools-preview
```bash
rustup component add llvm-tools-preview
```
install bootimage
```bash
# don't install in current directory
cargo install bootimage
```