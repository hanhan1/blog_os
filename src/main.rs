#![no_std]
#![no_main]

extern crate rlibc;

use core::panic::PanicInfo;


mod vga_buffer;

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {

    println!("{}", info);
    loop {}
}

#[no_mangle]
pub extern "C" fn _start() -> ! {
    let vga_buffer = 0xb8000 as *mut u8;

    println!("Numbers: {} {}", 1, 1.337);
    panic!("oops");
    loop {}
}

